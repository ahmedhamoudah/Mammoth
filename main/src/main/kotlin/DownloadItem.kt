import ninja.sakib.pultusorm.annotations.AutoIncrement
import ninja.sakib.pultusorm.annotations.PrimaryKey

class DownloadItem{

    @PrimaryKey
    @AutoIncrement
    var Id : Int = 0
    var categoryId: Int = 0
    var title : String? = null
    var url : String? = null
    var videoId : String? = null
    var tempDir : String = System.getProperty("java.io.tmpdir") + "/Downloads"
    var downloadDir : String = System.getProperty("user.home") + "/Downloads"
    var comment : String? = null
    var state : String = DownloadStates.WITHING.toString()
    var progress : Float = 0f




}