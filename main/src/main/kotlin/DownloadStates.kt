enum class DownloadStates(id : Int) {
    DOWNLOADING(1),
    PAUSED(2),
    ERROR(3),
    WITHING(4)
}