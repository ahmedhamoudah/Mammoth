import javafx.application.Application
import java.io.BufferedOutputStream
import java.net.ConnectException
import java.net.InetAddress
import java.net.Socket
import kotlin.concurrent.thread


fun main(args: Array<String>) {
    try {
        val socket = Socket(InetAddress.getLocalHost(), 4848)
        val bos = BufferedOutputStream(socket.getOutputStream())
        val request = Requests.OPEN_UI.command
        val buffer = ByteArray(1024)
        bos.write(request.toByteArray())
        bos.flush()
        bos.close()
        socket.close()
    }catch (e : ConnectException){
//        thread {
//            val serviceArg = ArrayList<String>()
//            serviceArg.add("Start Normal")
//            MammothService(this).start()
            Application.launch(Main::class.java)
//        }
    }

}