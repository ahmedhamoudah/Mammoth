import java.util.*
import kotlin.collections.ArrayList

class DownLoadTaskManager() : Thread() {
    val allTasks = ArrayList<DownloadTask>()

    private val queue : LinkedList<DownloadTask> = LinkedList()
    private val runningTasks = LinkedList<DownloadTask>()
    private val forceRunningTasks = LinkedList<DownloadTask>()
    val running = 3
    var isRunning = false


    init {

        DBManager().getItems().forEach { allTasks.add(DownloadTask(it)) }

    }

    override fun run() {

//        while (queue.size > 0){
//            isRunning = true
//            for (i in 0 until running){
//                if (queue.size <= 0)
//                    break
//                val task = queue.first
//                task.start()
//                runningTasks.add(task)
//                queue.remove(task)
//            }
//
//            while (true){
//                for (i in runningTasks){
//                    if (!i.isAlive){
//                        runningTasks.remove(i)
//                        if (queue.size <=0)
//                            break
//                        val task = queue.first
//                        task.start()
//                        runningTasks.add(task)
//                        queue.remove(task)
//
//                    }
//                }
//
//           }
//
//
//        }
//
//        isRunning = false

    }


    fun addTask(task: DownloadTask): Unit {
        queue.add(task)
        task.addOnAfterListener {
            if(queue.size > 0) {
                queue.remove(task)
                if (queue.isNotEmpty()) {
                    startNext()
                    isRunning = true
                }
            }

            if(queue.isEmpty())
                isRunning = false
        }
        if (!isRunning)
            task.start()


//        if (!isAlive && !isRunning)
//            start()
    }

    fun startNow(task: DownloadTask): Unit {
        if (queue.contains(task))
            queue.remove(task)
        forceRunningTasks.add(task)
        task.start()
    }

    fun stopTask(task: DownloadTask): Unit {

    }


    private fun startNext(){
        queue.first.start()
    }


}